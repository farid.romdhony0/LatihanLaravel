var getUrl = window.location.origin;

function load_bar(html) {
    var progress = setInterval(function() {
    var $bar = $('#bar-progress');
    var $value =  $('#prosen-loading');
    
    if ($bar.width()>=500) {
      
        // complete      
        clearInterval(progress);
        $('.progress').removeClass('active');
        $('#loading-modal-bar').modal('hide');
        $bar.width(0);
        $value.text("0 %");
        window.location.href = getUrl+"/hasil/view/"+html;
    } else { 
        // perform processing logic here
        $bar.width($bar.width()+150);
    }
    $value.text($bar.width()/5);
    }, 800);
}

$("#preview-form").submit(function(e){
    e.preventDefault();
    $.ajax({
        'type': 'POST',
        'url': getUrl+"/proses",
        'data': $(this).serialize(),
        'success': function(html){
            load_bar(html);
        }
    });
});


$("#bHasil").click(function() {
    var hasil = $(this).attr("value");
    console.log(hasil);
});

var menus =  $("#menus-choice").val();
$("#"+menus).addClass("active"); //Add "active" class to selected tab 


$("#login_admin_log").submit(function(e){
    e.preventDefault();
    $.ajax({
        'type': 'POST',
        'url': getUrl+"/admin/verification",
        'data': $(this).serialize(),
        'success': function(html){
            if (html!="dashboard") {
                // console.log(html);
                $("#alert-login-admin").css("display", "block");
            }else{
                window.location.href = getUrl+"/admin/"+html;
            }
        }
    });
});

$("#verifikasiLogin").submit(function(e){
    e.preventDefault();
    $.ajax({
        'type': 'POST',
        'url': getUrl+"/verifikasi/login",
        'data': $(this).serialize(),
        'success': function(html){
            if ($.isNumeric(html)) {
                console.log("success");
                window.location.href=getUrl+"/profil/view/"+html;
            }else{
                $("#the-message").text(html);
                $("#alert-login-member").css("display", "block");
            }
        }
    });
});

$("#verifikasiRegis").submit(function(e){
    e.preventDefault();
    $.ajax({
        'type': 'POST',
        'url': getUrl+"/register",
        'data': $(this).serialize(),
        'success': function(html){
            console.log(html);
            $("#the-message").text(html);
            $("p.lead").css("display","none");
            $("#alert-regis-member").css("display", "block");
        }
    });
});


$("#add-gejala").click(function(){
    console.log('success');
    // $("#primary_gejala").append("<div class='col-md-12'>AAA</div>");
    var jumlah =$('#jumlah_gejala').val();
    for (var i = 0; i < (jumlah-1); i++) {
        var data = '<div class="panel panel-default m-leftright-20">'+
                        '<div class="panel-heading" style="padding: 10px 15px;">'+
                            '<h4 style="margin: 0px;"><i class="fa fa-book"></i> Gejala '+(i+2)+'</h4>'+
                        '</div>'+
                        '<div class="panel-body" style="padding: 5px;">'+
                            '<div class="col-md-2 del-col">'+
                                '<select id="selectKategori" name="kategori[]" class="form-control"></select>'+
                            '</div>'+
                            '<div class="col-md-10 del-col">'+
                                '<input type="text" name="dataGejala[]" class="form-control" placeholder="nama gejala">'+
                            '</div>'+
                        '</div>'+
                    '</div>';
        $("#add-gejala").attr("disabled","disabled");
        $("#primary_gejala").append(data);
    }
    $.ajax({
        'type': 'GET',
        'url': getUrl+"/admin/ketegori/get",
        'success': function(html){
            var decode = JSON.parse(html);
            console.log(decode);
            for (var i = 0; i < decode.length; i++) {
                $option = '<option value="'+decode[i]['id_kategori']+'">'+decode[i]['nama_kategori']+'</option>';
                $('[id=selectKategori]').append($option);
            }
        }
    });
});

$('#logout-button').click(function(){
    $.ajax({
        'type': 'POST',
        'url': getUrl+"/unsetPremission",
        'success': function(html){
            $('#logout-button').text('Login');
        }
    });
});

$("#form-profil").submit(function(e){
    e.preventDefault();
    $.ajax({
        'type': 'POST',
        'url': getUrl+"/profil/save",
        'data': $(this).serialize(),
        'success': function(html){
            if (html=="true") {
                window.location.href = getUrl+"/profil";
            }else{
                // $("#alert-login-member").text(html);
                $("#alert-profil").css("display", "block");
            }
        }
    });
});