<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/list', 'Cuser@listData');
Route::get('/', 'Cuser@listData');

Route::get('/user', 'Cuser@userManajement');
Route::get('/user/{action}/{id}', 'Cuser@userManajement')
		->where('action', '[A-Za-z]+')
		->where('id','[0-9]+');

Route::get('/data', 'Cuser@manajementData');
Route::get('/data/{action}/{id}', 'Cuser@manajementData')
		->where('action', '[A-Za-z]+')
		->where('id','[0-9]+');
Route::get('/data/{action}', 'Cuser@manajementData')
		->where('action', '[A-Za-z]+');

Route::post('/datasave','Cuser@saveData');
Route::post('/updatesave','Cuser@editData');
// Route::post('/data')
Route::post('/updateuser','Cuser@editUser');

Route::get('/login','Cuser@login');
Route::post('login/verifikasi','Cuser@verifikasiLogin');

Route::get('/logout','Cuser@delSession_login');
