<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Mdata;
use Session;
use Input;

class Cuser extends Controller
{
    private function load_page($content,$data=null){
    	$data[] = '';
        return view('content/'.$content.'',$data);
    }

    public function listData()
    {
        $level =  $this->userPresmission('list');

        if ($level != true) {
        	echo "ada tidak dapat mengakses fitur ini";
        	return;	
        }

        $data['data'] = DB::table('data_transaksi')->get();
		return $this->load_page('hasilList',$data);
    }

    public function manajementData($action=null,$id=null){
    	$level =  $this->userPresmission('DataManajement');
        if ($level != true) {
        	echo "ada tidak dapat mengakses fitur ini";
        	return;	
        }

    	if (empty($action)) {
        	$data['data'] = DB::table('data_transaksi')->get();
        	// dd($data);
        	return $this->load_page('hasilData',$data);
        }elseif($action=='add'){
        	return $this->load_page('addData');
        }elseif ($action=='edit') {
        	$data['data'] = Mdata::singleData($id);
        	// dd($data);
        	return $this->load_page('addData',$data);
        }elseif ($action=='delete') {
        	DB::table('data_transaksi')->where('id_data', $id)->delete();
        	return redirect('data');
        }
    }

    public function saveData(){
    	$name = request()->post('name');
    	// dd($name);
    	DB::table('data_transaksi')->insert(
		    ['name_data' => $name]
		);
		return redirect('data');
    }

    public function editData(){
    	$post = request()->post();
    	DB::table('data_transaksi')
            ->where('id_data', $post['id'])
            ->update(['name_data' => $post['name']]);

        return redirect('data');
    }
    public function editUser(){
    	$post = request()->post();
    	DB::table('user_premission')
            ->where('user_id', $post['id'])
            ->update(['premission' => $post['premission']]);

        return redirect('user');
    }

    public function userManajement($action=null,$id=null){
    	$level =  $this->userPresmission('UserManajement');
        if ($level != true) {
        	echo "ada tidak dapat mengakses fitur ini";
        	return;	
        }
	

        if (empty($action)) {
        	$data['data'] = DB::table('user_premission')->get();
        	// dd($data);
        	return $this->load_page('hasilUser',$data);
        }elseif ($action=='edit') {
        	$data['data'] = Mdata::singleUser($id);
        	// dd($data);
        	return $this->load_page('addUser',$data);
        }elseif ($action=='delete') {
        	DB::table('user_premission')->where('user_id', $id)->delete();
        	return redirect('user');
        }

        
    }


    public function login(){
    	$data[] = '';
    	return $this->load_page('login',$data);
    }

    private function userPresmission($level){
    	$premission = $this->getSessionLogin();
    	switch ($level) {
    		case 'list':
    			if ($premission['premission'] == 1 || $premission['premission'] == 4)   {
    				return true;
    			}else{
    				return false;
    			}
    			break;
    		case 'UserManajement':
    			if ($premission['premission'] == 2 || $premission['premission'] == 4) {
    				return true;
    			}else{
    				return false;
    			}
    			break;
    		case 'DataManajement':
    			if ($premission['premission'] == 3 || $premission['premission'] == 4) {
    				return true;
    			}else{
    				return false;
    			}
    			break;
    		default:
    			# code...
    			break;
    	}
    }

    private function getSessionLogin(){
        $data = array(
            'premission' => Session::get('user_premission'),
            'ids' => Session::get('user_id') 
        );
        return $data;
    }

    public function verifikasiLogin(){
    	$login = request()->post();
    	$nama = $login['name'];
    	$password = $login['password'];
        $status = Mdata::singleLogin($nama);

        if ($status != false) {
            // $mdPass = md5($password);
            if ($password == $status['password']) {
                Session::put('user_premission',$status['premission']);
                Session::put('user_id',$status['id']);
                return redirect('list');
            }else{
                echo "Password salah silahkan masukkan ulang";die;  
            }
        }else{
            echo "Anda belum terdaftar. Silahkan daftar !";die;
        }
    }
    public function delSession_login(){
        Session::forget('user_premission');
        Session::forget('user_id');
        return redirect('login');
    }
}
