<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Mdata extends Model
{
    static function singleLogin($nama){
		$login = DB::table('user_premission')
				->select('user_id','premission','password')
				->where('name_user',$nama)
				->get();
		// dd($login);
		if (!$login->isEmpty() && count($login)==1) {
			$return = array(
				'id' => $login[0]->user_id,
				'password'=>$login[0]->password,
				'premission' => $login[0]->premission
			);
	        return $return;
		}else{
			return false;
		}
	}

	public static function singleUser($id){
		$data = DB::table('user_premission')
				->select('*')
				// ->join('solusi','solusi.id_kerusakan','=','kerusakan.id')
				->where('user_id',$id)
				->get();
		
		return $data;
	}
	public static function singleData($id){
		$data = DB::table('data_transaksi')
				->select('*')
				// ->join('solusi','solusi.id_kerusakan','=','kerusakan.id')
				->where('id_data',$id)
				->get();
		
		return $data;
	}
}
