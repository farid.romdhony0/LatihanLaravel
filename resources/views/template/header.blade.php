        
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="brand" style="padding: 25px 15px;">
                <a href="index.html"><img src="{{ URL::asset('img/logos-dark.png')}}" alt="Klorofil Logo" class="img-responsive logo"></a>
            </div>
            <div class="container-fluid">
                <div class="navbar-btn">
                    <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
                </div>
                <form class="navbar-form navbar-left">
                    <div class="input-group">
                        <input type="text" value="" class="form-control" placeholder="Search kerusakan...">
                        <span class="input-group-btn"><button type="button" class="btn btn-primary">Go</button></span>
                    </div>
                </form>
                <div class="navbar-btn navbar-btn-right">
                    <a class="btn btn-success update-pro" href="{{url('logout')}}"><i class="fa fa-user"></i> <span>Logout</span></a>
                </div>
                
            </div>
        </nav>