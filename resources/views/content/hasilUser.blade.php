@extends('template.index')

@section('content')
<div class="container-fluid">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<h3>List Hasil Identifikasi 
					<!-- <button class="btn btn-succes">Tambah</button> -->
				</h3>

			</div>
			<div class="col-md-12 panel panel-headline" style="min-height: 200px;">
				<table class="table table-striped" style="margin: 10px 0px;">
					<thead>
						<th><i class="fa fa-list"></i> No</th>
						<th><i class="fa fa-user"></i> Nama</th>
						<th><i class="fa fa-calendar"></i> Premission</th>
						<th><i class="fa fa-bookmark"></i> Action</th>
					</thead>
					<tbody>
						<?php $num=1; ?>
						@foreach($data as $dt)
						<tr>
							<td><?=$num++?></td>
							<td><?=$dt->name_user?></td>
							<td><?=$dt->premission?></td>
							<td>
								<a href="{{url('user/edit').'/'.$dt->user_id}}"><button class="btn btn-succes btn-table-act"><i class="fa fa-eye"></i></button></a>
								<a href="{{url('user/delete').'/'.$dt->user_id}}"><button class="btn btn-succes btn-table-act"><i class="fa fa-trash"></i></button></a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>		
		</div>
	</div>	
</div>
@stop