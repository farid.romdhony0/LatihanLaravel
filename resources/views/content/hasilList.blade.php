@extends('template.index')

@section('content')
<div class="container-fluid">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<h3>List Hasil Identifikasi</h3>
			</div>
			<div class="col-md-12 panel panel-headline" style="min-height: 200px;">
				<table class="table table-striped" style="margin: 10px 0px;">
					<thead>
						<th><i class="fa fa-list"></i> No</th>
						<th><i class="fa fa-user"></i> Nama</th>
						
					</thead>
					<tbody>
						<?php $num=1; ?>
						@foreach($data as $dt)
						<tr>
							<td><?=$num++?></td>
							<td><?=$dt->name_data?></td>
							
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>		
		</div>
	</div>	
</div>
@stop