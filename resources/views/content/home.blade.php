@extends('template.index')

@section('content')

	<div class="container-fluid">
		<div class="panel panel-headline">
            <div class="panel-heading">
                <h3 class="panel-title">Admin Expert System</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="metric">
                            <span class="icon"><i class="fa fa-archive"></i></span>
                            <p>
                                <span class="number">{{$widget['kerusakan']}}</span>
                                <span class="title">Kerusakan</span>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="metric">
                            <span class="icon"><i class="fa fa-book"></i></span>
                            <p>
                                <span class="number">{{$widget['gejala']}}</span>
                                <span class="title">Gejala</span>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="metric">
                            <span class="icon"><i class="fa fa-users"></i></span>
                            <p>
                                <span class="number">{{$widget['member']}}</span>
                                <span class="title">Member</span>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="metric">
                            <span class="icon"><i class="fa fa-calendar-check-o"></i></span>
                            <p>
                                <span class="number">{{$widget['hasil']}}</span>
                                <span class="title">Hasil</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                    	<div class="grid-table-admin">
                    		<table class="table-admin table table-striped">
                    			<div class="col-md-12 del-col" style="margin-bottom: 10px;">
                    				<div class="col-md-3 del-col">
                    					<h5 style="padding: 0px 10px;">
	                    					<i class="fa fa-tasks"></i> List Kerusakan
	                    				</h5>	
                    				</div>
                    				<div class="col-md-9 del-col">
                    					<a href="{{url('admin/kerusakan/add')}}">
	                    					<button type="button" class="btn btn-success update-pro" style="padding: 3px 15px;"><i class="fa fa-plus-square"></i> Tambah</button>
	                    				</a>	
                    				</div>
                    			</div>
	                        	<thead>
	                        		<th style="width: 15%;"><i class="fa fa-list"></i> No</th>
	                        		<th style="width: 45%;"><i class="fa fa-building-o"></i> Nama</th>
	                        		<th><i class="fa fa-bookmark"></i> Gejala kerusakan</th>
	                        		<th></th>
	                        	</thead>
								<tbody>
									<?php $num=1; ?>
									@foreach($kerusakan as $dt)
									<tr>
										<td>{{$num++}}</td>
										<td>{{$dt->nama_kerusakan}}</td>
										<td><span class="label label-info">{{$dt->gejala}} gejala</span></td>
										<td>
											<a href="{{url('kerusakan/view').'/'.$dt->id}}">
												<button class="btn btn-succes btn-table-act"><i class="fa fa-eye"></i></button>
											</a>
											<a href="#">
												<button class="btn btn-info btn-table-act"><i class="fa fa-edit"></i></button>
											</a>
											<a href="{{url('admin/kerusakan/').'/'.$dt->id}}">
												<button class="btn btn-danger btn-table-act"><i class="fa fa-trash"></i></button>
											</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
                    	</div>

                    	<div class="grid-table-admin">
                    		<table class="table-admin table table-striped">
	                        	<h5 style="padding: 0px 10px;"><i class="fa fa-tasks"></i> List Member</h5>
	                        	<thead>
	                        		<th style="width: 15%;"><i class="fa fa-list"></i> No</th>
	                        		<th style="width: 45%;"><i class="fa fa-user"></i> Nama Member</th>
	                        		<th><i class="fa fa-calendar"></i> Tanggal Masuk</th>
	                        		<th></th>
	                        	</thead>
								<tbody>
									<?php $num=1; ?>
									@foreach($member as $dt)
									<tr>
										<td>{{$num++}}</td>
										<td>{{$dt->nama_member}}</td>
										<td><span class="label label-info">{{$dt->tanggal_daftar}}</span></td>
										<td>
											<a href="{{url('profil/view/').'/'.$dt->id_member}}">
												<button class="btn btn-succes btn-table-act"><i class="fa fa-eye"></i></button>
											</a>
											<a href="{{url('admin/member/').'/'.$dt->id_member}}">
												<button class="btn btn-danger btn-table-act"><i class="fa fa-trash"></i></button>
											</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
                    	</div>

                    	<div class="grid-table-admin">
                    		<table class="table-admin table table-striped">
	                        	<h5 style="padding: 0px 10px;"><i class="fa fa-tasks"></i> List Hasil</h5>
	                        	<thead>
	                        		<th style="width: 15%;"><i class="fa fa-list"></i> No</th>
	                        		<th style="width: 45%;"><i class="fa fa-user"></i> Nama Hasil</th>
	                        		<th><i class="fa fa-calendar"></i> Tanggal</th>
	                        		<th></th>
	                        	</thead>
								<tbody>
									<?php $num=1;?>
									@foreach($hasil as $dt)
									<tr>
										<td>{{$num++}}</td>
										<td>{{$dt->nama_hasil}}</td>
										<td><span class="label label-info">{{$dt->tanggal_hasil}}</span></td>
										<td>
											<a href="{{url('hasil/view').'/'.$dt->id_hasil}}">
												<button class="btn btn-succes btn-table-act"><i class="fa fa-eye"></i></button>
											</a>
											<a href="{{url('admin/hasil/').'/'.$dt->id_hasil}}">
												<button class="btn btn-danger btn-table-act"><i class="fa fa-trash"></i></button>
											</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
                    	</div>
						
                    </div>
                    <div class="col-md-3">
                        <ul class="list-unstyled activity-list">
                        	<li class="log-title">
                                <h4>Log Proses</h4>
                            </li>
                            <!-- Perulangan -->
                            @foreach($log as $lg)
                            <li style="padding: 15px 0px;">
                                {{$lg->jenis_proses}}
                                <span class="timestamp">{{$lg->nama_member}}</span>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
	</div>








@stop