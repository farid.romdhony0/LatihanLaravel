@extends('template.index')

@section('content')
<div class="container-fluid">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<h3>List Hasil Identifikasi <a href="{{url('data/add')}}"><button class="btn btn-succes">Tambah</button></a></h3>

			</div>
			<div class="col-md-12 panel panel-headline" style="min-height: 200px;">
				<table class="table table-striped" style="margin: 10px 0px;">
					<thead>
						<th><i class="fa fa-list"></i> No</th>
						<th><i class="fa fa-user"></i> Nama Data</th>
						<th><i class="fa fa-calendar"></i> Action</th>
						
					</thead>
					<tbody>
						<?php $num=1; ?>
						@foreach($data as $dt)						
						<tr>
							<td><?=$num++?></td>
							<td><?=$dt->name_data?></td>
							
							<td>
								<a href="{{url('data/edit').'/'.$dt->id_data}}"><button class="btn btn-succes btn-table-act"><i class="fa fa-eye"></i></button></a>
								<a href="{{url('data/delete').'/'.$dt->id_data}}"><button class="btn btn-succes btn-table-act"><i class="fa fa-trash"></i></button></a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>		
		</div>
	</div>	
</div>
@stop