@extends('template.index')

@section('content')
	<div class="container-fluid">
		<div class="panel panel-headline">
			

			<div class="panel-body">
				<div class="row">
					@if(!empty($data[0]->name_data))
					<form id="addPenyakitDat" action="{{url('/updatesave')}}" class="form-horizontal style-form" method="POST">
					@else
					<form id="addPenyakitDat" action="{{url('/datasave')}}" class="form-horizontal style-form" method="POST">
					@endif
					
						{{csrf_field()}}
	                  	<div class="form-group">
	                      	<label class="lebel-add col-md-2 control-label"> Data Name </label>
	                      	<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-building"></i></span>
								@if(!empty($data[0]->name_data))
								<input type="hidden" name="id" value="<?=$data[0]->id_data?>"></ins>
								<input name="name" class="form-control" placeholder="Nama kerusakan" type="text" value="<?=$data[0]->name_data?>">
								@else
								<input name="name" class="form-control" placeholder="Nama kerusakan" type="text" >
								@endif
							</div>
	                  	</div>
	              
	                 
	                  	<button type="submit" class="btn btn-primary"><i class="fa fa-external-link-square"></i> Simpan</button>
	              	</form>
				</div>

			</div>
		</div>
	</div>
	


@stop