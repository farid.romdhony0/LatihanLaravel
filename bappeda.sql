-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 22 Jul 2018 pada 16.08
-- Versi Server: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bappeda`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_transaksi`
--

CREATE TABLE `data_transaksi` (
  `id_data` int(11) NOT NULL,
  `name_data` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_transaksi`
--

INSERT INTO `data_transaksi` (`id_data`, `name_data`) VALUES
(1, 'Data 1'),
(2, 'data dua'),
(3, 'Data 3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `premission_level`
--

CREATE TABLE `premission_level` (
  `premission_id` int(11) NOT NULL,
  `nama` varchar(1000) NOT NULL,
  `level_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_premission`
--

CREATE TABLE `user_premission` (
  `user_id` int(11) NOT NULL,
  `name_user` varchar(1000) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `premission` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_premission`
--

INSERT INTO `user_premission` (`user_id`, `name_user`, `password`, `premission`) VALUES
(1, 'Administrator', '1234', 2),
(2, 'SuperAdmin', '1234', 4),
(3, 'Admin', '1234', 3),
(4, 'Direksi', '1234', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_transaksi`
--
ALTER TABLE `data_transaksi`
  ADD PRIMARY KEY (`id_data`);

--
-- Indexes for table `premission_level`
--
ALTER TABLE `premission_level`
  ADD PRIMARY KEY (`premission_id`);

--
-- Indexes for table `user_premission`
--
ALTER TABLE `user_premission`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_transaksi`
--
ALTER TABLE `data_transaksi`
  MODIFY `id_data` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `premission_level`
--
ALTER TABLE `premission_level`
  MODIFY `premission_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_premission`
--
ALTER TABLE `user_premission`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
